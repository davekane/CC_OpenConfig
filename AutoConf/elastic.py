import datetime
import os
import subprocess
from tempfile import mkstemp
from shutil import move
from os import remove, close
from random import randint

#Ersetze vordefinierte Customer ID mit aktuell berechneter
def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,"w") as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

def getAndReplaceIP():
    local_ip = subprocess.check_output(["hostname", "-I"])
    local_ip = str(local_ip)
    local_ip = local_ip.replace("b","")
    local_ip = local_ip.replace("\\n","")
    local_ip = local_ip.replace("\'","")
    if "$AutoIP" in open('/home/mininet/test.txt').read():
        replace("/home/mininet/test.txt","$AutoIP",local_ip)

replace("/etc/default/elasticsearch", "#MAX_LOCKED_MEMORY=unlimited", "MAX_LOCKED_MEMORY=unlimited")
replace("/usr/lib/systemd/system/elasticsearch.service", "#LimitMEMLOCK=infinity", "LimitMEMLOCK=infinity")
#getAndReplaceIP()
