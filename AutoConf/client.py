import datetime
import os
import subprocess
from tempfile import mkstemp
from shutil import move
from os import remove, close
from random import randint

#print subprocess.check_output(["git", "clone", "https://gitlab.com/davekane/CC_OpenConfig"])

#Berechne Client-ID und aktuelles Datum
custID = randint(0,9999999999)
today = datetime.date.today()
custDate = today.strftime("%d%m%y")
fullID = "[" + str(custID) + "-" + str(custDate) + "]"

#Ersetze vordefinierte Customer ID mit aktuell berechneter
def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,"w") as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

replace("/home/ubuntu/config/Rsyslog/rsyslog_apache.conf", "[0000000001-07062017]", fullID)
replace("/home/ubuntu/config/Rsyslog/rsyslog_kafka.conf", "[0000000001-07062017]", fullID)
replace("/home/ubuntu/config/Rsyslog/rsyslog_tcp.conf", "[0000000001-07062017]", fullID)

os.system("mv /home/ubuntu/config/Rsyslog/rsyslog_apache.conf /etc/rsyslog.d/")
os.system("mv /home/ubuntu/config/Rsyslog/rsyslog_kafka.conf /etc/rsyslog.d/")
os.system("mv /home/ubuntu/config/Rsyslog/rsyslog_tcp.conf /etc/rsyslog.d/")
