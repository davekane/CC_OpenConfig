import sys
import mysql.connector

cnx = mysql.connector.connect(user='accounter', password='accounting',
                              host='127.0.0.1',
                              database='accounting')
print ("Connected")

def customerExists (customer):
    try:
        cursor = cnx.cursor()
        query = "select * from customer where custID = '" + customer + "'"
        cursor.execute(query)
        rowCounter = 0

        for (custID, dataSize, billing) in cursor:
          rowCounter += 1

        cursor.close()
        return rowCounter;
    except Exception as ex:
        print ("Error at method customerExists: " + str(ex))

def insertNew (customer, dataSize, billing):
    try:
        cursor = cnx.cursor()
        localDataSize = dataSize / 1000000
        dbBillValue = localDataSize * 10

        query = "INSERT INTO customer (custID, dataSize, billing) VALUES (%s, %s, %s)"
        values = (customer, localDataSize, dbBillValue)

        cursor.execute(query, values)
        emp_no = cursor.lastrowid

        cnx.commit()
        cursor.close()
    except Exception as ex:
        print ("Error at method insertNew: " + str(ex))

def updateExists (customer, dataSize):
    try:
        #Filled with current costumer data size from DB.
        dbDataSize = 0.0
        #Data size count per MB. Parameter contains only bytes. Calculate MB.
        localDataSize = float(dataSize) / 1000000
        #Filled with current amount of Euro which the customer has to pay
        dbBillValue = 0.0

        print (str(dbDataSize) + ", " + str(localDataSize) + ", " + str(dbBillValue))

        cursor = cnx.cursor()
        query = "select * from customer where custID = '" + customer + "'"
        cursor.execute(query)

        for (custID, dataSizeDB, billing) in cursor:
          #dbDataSize = float("{}".format(dataSize))
          dbDataSize = float(dataSizeDB)
          #dbBillValue = float("{}".format(billing))
          dbBillValue = float(billing)

        #Update the current data size the customer has sent to our service
        #print ("First: " + str(dbDataSize) + ", " + str(localDataSize))
        dbDataSize = dbDataSize + localDataSize
        #Update the current amount the customer has to pay for our service
        #print ("Second: " + str(dbBillValue) + ", " + str((localDataSize * 10)))
        dbBillValue = (dbBillValue + (localDataSize * 10))

        updateQuery = "update customer set dataSize = %s, billing = %s where custID = %s"
        values = (dbDataSize, dbBillValue, customer)
        cursor.execute(updateQuery,values)
        cnx.commit()
        cursor.close()
    except Exception as ex:
        print ("Error at method updateExists: " + str(ex))

def closeConn():
    cnx.close()

#Check if customer already has sent any logs to our service
def customerInput(customerID, logLength):
    if customerExists(customerID) == 1:
        updateExists(str(customerID), logLength)
    elif customerExists(customerID) == 0:
        insertNew(str(customerID), logLength, 0)
