import socket
import threading
import re
import sys
from DBFunctionality import customerInput
import mysql.connector
import subprocess

fullMessage     = "(\{((.){1,})\})"
custIDPattern   = "((\[){0,1})(([0-9]{10})\-([0-9]{8}))((\]){0,1})"

def getAndReplaceIP():
    local_ip = subprocess.check_output(["hostname", "-I"])
    return local_ip

host = getAndReplaceIP()
port = 12345
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((host, port))

def listen():
    #Maximale Connections in der Queue
    sock.listen(5)
    while True:
        client, address = sock.accept()
        client.settimeout(60)
        threading.Thread(target = listenToClient,args = (client,address)).start()

def listenToClient(client, address):
    size = 2048
    while True:
        try:
            data = client.recv(size)
            if data:
                #Wertet die einzelnen Customer aus
                getCustID(str(data))
        except Exception as ex:
            print (ex)
            client.close()
            return False

def getCustID(log):
    fullLog = searchPattern(fullMessage, log)
    mo = re.search("((\[){0,1})(([0-9]{10})\-([0-9]{8}))((\]){0,1})", fullLog)

    customer = str(mo.group())
    logLength = utf8len(fullLog)

    customerInput("[0000000002-07172017]", logLength)

def searchPattern(pattern, string):
    regOne = re.search(pattern, string)
    if regOne != None:
        return str(regOne.group())

def utf8len(s):
    return str(len(s.encode('utf-8')))


#Lets start the accounting
listen()
