#!/usr/bin/env python

#ToDo:
#- Anpassung des Scripts zur Realisierung von mehreren Brokern auf einem Node
#	- Anlegen von /tmp directory für broker
#	- Entsprechende Anpassungen in serverRuntime.properties realisieren
#		- host.name muss individuell sein
#		- Port für broker muss individuell sein

import os
import sys
import subprocess
import shlex
import logging
import re
import netifaces as ni
from kazoo.client import KazooClient

logging.basicConfig()

ip_znode = '192.168.0.193'
zk = KazooClient(hosts=ip_znode+':2181')
zk.start()
try:
    #GET ID for broker.id
    try:
        zk_broker_ids = zk.get_children('/brokers/ids')
        zk.stop()
        set_broker_ids = set(map(int, zk_broker_ids))
        possible_broker_ids = set(range(100))
        broker_id = sorted(possible_broker_ids - set_broker_ids)[0]
        propertie_broker_id = 'broker.id='+str(broker_id)
        print("Succesful get broker.id")
    except:
        propertie_broker_id = 'broker.id=0'
        print("ERROR: Something went wrong while allocating broker.id - Default of 0 will be used !!!")

    #GET IP as host.name
    try:
        propertie_broker_hostName = 'host.name='+ni.ifaddresses('ens3')[2][0]['addr']
        propertie_broker_znode = 'zookeeper.connect='+ip_znode
        print("Succesful get host.name")
    except:
        print("ERROR: Something went wrong while allocating host.name - Broker can not be start with this state !!!")
        sys.exit()

    #Read the Content from properties file an add the new broker id
    try:
        with open ('/opt/Kafka/kafka/config/serverRuntime.properties', 'r' ) as f:
            content = f.read()
            content_1 = re.sub('broker.id=(\d{1})', propertie_broker_id, content, flags = re.M)
            content_2 = re.sub('host.name=(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})', propertie_broker_hostName, content_1, flags = re.M)
            content_3 = re.sub('zookeeper.connect=(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})', propertie_broker_znode, content_2, flags = re.M)
            print("Succesful created new content")
    except:
        print("ERROR: Something went wrong while reading serverRuntime.properties")
        sys.exit()

    try:
        #Write the new Content to the file
        print(content_3)
        output = open('/opt/Kafka/kafka/config/serverRuntime.properties', 'w')
        output.write(content_3)
        output.close()
        print("Succesful wrote new content")
    except:
        print("ERROR: Something went wrong while writing new serverRuntime.properties")
        sys.exit()

    print("Broker properties updated. Broker will be started with "+ propertie_broker_id +" and with "+propertie_broker_hostName)
    subprocess.call(shlex.split('./bin/kafka-server-start.sh config/serverRuntime.properties'))

except:
    print("ERROR: Something went wrong while allocating propertie values !!!")

sys.exit()
