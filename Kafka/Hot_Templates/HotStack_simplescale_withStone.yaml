########################################################################################################################
########################################################################################################################
#Parameter#
########################################################################################################################
########################################################################################################################

heat_template_version: 2013-05-23
description: AutoScaling Simple
parameters:
  image:
    type: string
    description: Image used for servers
    default: Ubuntu16.04-XenialXerus-64bit-CloudBasedImage
  flavor1:
    type: string
    description: flavor used by the web servers
    default: m1.small
  flavor2:
    type: string
    description: flavor used by the web servers
    default: m1.large
  public_net_id:
    type: string
    description: network on which the instances
    default: CloudComp3-net
  key_name:
    type: string
    description: Name of an existing key pair to use for the template
    default: keypairtim
  security_group_id:
    type: string
    description: Security group used for the servers
    default: default

########################################################################################################################
########################################################################################################################
#AutoScalingGroup#
########################################################################################################################
########################################################################################################################

resources:
# auto_scale_group autoscale group and it's init contents to generate load
  auto_scale_group:
    type: OS::Heat::AutoScalingGroup
    properties:
      min_size: 0
      max_size: 6
      resource:
        type: OS::Nova::Server
        properties:
          flavor: {get_param: flavor1}
          image: {get_param: image}
          key_name: {get_param: key_name}
          metadata: {"metering.stack": {get_param: "OS::stack_id"}}
          networks:
            - network: { get_param: public_net_id }
          config_drive: True
          security_groups: [get_param: security_group_id]
          user_data_format: RAW
          user_data: |
            #cloud-config
            # this file is for a new Broker Node

            manage_etc_hosts: true

            apt_sources:
             # add Oracle's PPA
             - source: "ppa:webupd8team/java"

            bootcmd:
             # this line solved the problem which occured while installing java
             # Error which appears in log: oracle-license-v1-1 license could not be presented
             - cloud-init-per once accepted-oracle-license-v1-1 echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections

            # Update package respository
            package_update: true

            packages:
             # install latest stable version of Java (Oracle JDK 8)
             - oracle-java8-installer
             - oracle-java8-set-default
             - python3-pip

            runcmd:
             # get the latest version of Apache Kafka and extract the .tgz to new location /opt/Kafka/
             - wget http://apache.mirror.digionline.de/kafka/0.10.2.1/kafka_2.12-0.10.2.1.tgz -P /tmp
             - sudo mkdir /opt/Kafka
             - sudo tar -xzf /tmp/kafka_2.12-0.10.2.1.tgz -C /opt/Kafka
             - sudo mv /opt/Kafka/kafka_2.12-0.10.2.1 /opt/Kafka/kafka
             - cd /opt/Kafka/kafka
             # Get Files from GitLab
             - git clone https://gitlab.com/davekane/CC_OpenConfig
             - sudo cp CC_OpenConfig/Kafka/KafkaNode/pythonScript_brokerInit.py brokerInit.py
             - sudo cp CC_OpenConfig/Kafka/KafkaNode/kafkaConfig_serverRuntime.properties config/serverRuntime.properties
             # Install Python packages
             - sudo pip3 install netifaces
             - sudo pip3 install kazoo
             # Start KafkaNode
             - sudo python3 brokerInit.py

########################################################################################################################
########################################################################################################################
#Persistente Einheit#
########################################################################################################################
########################################################################################################################

  persistent_instance:
    type: OS::Nova::Server
    properties:
      flavor: {get_param: flavor2}
      image: {get_param: image}
      key_name: {get_param: key_name}
      metadata: {"metering.stack": {get_param: "OS::stack_id"}}
      networks:
        - network: { get_param: public_net_id }
      config_drive: True
      security_groups: [get_param: security_group_id]
      user_data_format: RAW
      user_data: |
        #cloud-config
        # this file is for a new Broker Node

        manage_etc_hosts: true

        apt_sources:
         # add Oracle's PPA
         - source: "ppa:webupd8team/java"

        bootcmd:
         # this line solved the problem which occured while installing java
         # Error which appears in log: oracle-license-v1-1 license could not be presented
         - cloud-init-per once accepted-oracle-license-v1-1 echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections

        # Update package respository
        package_update: true

        packages:
         # install latest stable version of Java (Oracle JDK 8)
         - oracle-java8-installer
         - oracle-java8-set-default
         - python3-pip

        runcmd:
         # get the latest version of Apache Kafka and extract the .tgz to new location /opt/Kafka/
         - wget http://apache.mirror.digionline.de/kafka/0.10.2.1/kafka_2.12-0.10.2.1.tgz -P /tmp
         - sudo mkdir /opt/Kafka
         - sudo tar -xzf /tmp/kafka_2.12-0.10.2.1.tgz -C /opt/Kafka
         - sudo mv /opt/Kafka/kafka_2.12-0.10.2.1 /opt/Kafka/kafka
         - cd /opt/Kafka/kafka
         # Get Files from GitLab
         - git clone https://gitlab.com/davekane/CC_OpenConfig
         - sudo cp CC_OpenConfig/Kafka/KafkaNode/pythonScript_brokerInit.py brokerInit.py
         - sudo cp CC_OpenConfig/Kafka/KafkaNode/kafkaConfig_serverRuntime.properties config/serverRuntime.properties
         # Install Python packages
         - sudo pip3 install netifaces
         - sudo pip3 install kazoo
         # Start KafkaNode
         - sudo python3 brokerInit.py

########################################################################################################################
########################################################################################################################
#Policies#
########################################################################################################################
########################################################################################################################

  server_scaleup_policy:
    type: OS::Heat::ScalingPolicy
    properties:
      adjustment_type: change_in_capacity
      auto_scaling_group_id: {get_resource: auto_scale_group}
      cooldown: 300
      scaling_adjustment: 1
  server_scaledown_policy:
    type: OS::Heat::ScalingPolicy
    properties:
      adjustment_type: change_in_capacity
      auto_scaling_group_id: {get_resource: auto_scale_group}
      cooldown: 300
      scaling_adjustment: -1

########################################################################################################################
########################################################################################################################
#CPU Alarms#
########################################################################################################################
########################################################################################################################

  cpu_alarm_high:
    type: OS::Ceilometer::Alarm
    properties:
      description: Scale-up if the average CPU > 50% for 5 minute
      meter_name: cpu_util
      statistic: avg
      period: 300
      evaluation_periods: 1
      threshold: 50
      alarm_actions:
        - {get_attr: [server_scaleup_policy, alarm_url]}
      matching_metadata: {'metadata.user_metadata.stack': {get_param: "OS::stack_id"}}
      comparison_operator: gt
  cpu_alarm_low:
    type: OS::Ceilometer::Alarm
    properties:
      description: Scale-down if the average CPU < 1% for 5 minute
      meter_name: cpu_util
      statistic: avg
      period: 300
      evaluation_periods: 1
      threshold: 15
      alarm_actions:
        - {get_attr: [server_scaledown_policy, alarm_url]}
      matching_metadata: {'metadata.user_metadata.stack': {get_param: "OS::stack_id"}}
      comparison_operator: lt
