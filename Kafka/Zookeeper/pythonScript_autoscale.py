#!/usr/bin/env python

import os
import sys
import subprocess
import shlex
import logging
import re
import netifaces as ni
from kazoo.client import KazooClient
import time
import math

logging.basicConfig()

old_reassign_suggestion = ""

replication_factor = 1

while 1:
  try:
    # Output der letzten Periode aus nohup.out loeschen
    output_flush_nohup = subprocess.getoutput("cat /dev/null > $HOME/nohup.out")

    # Verbindung zu Zookeeper herstellen
    zk = KazooClient(hosts='localhost:2181')
    zk.start()

    # Informationen ueber vorhandene Broker IDs erlangen
    zk_broker = zk.get_children('/brokers/ids')

    zk_topic_sylog = zk.get_children('/brokers/topics/syslog/partitions')
    zk_topic_apache = zk.get_children('/brokers/topics/apache/partitions')
    zk.stop()

    set_broker = set(map(int, zk_broker))
    set_topic_syslog = set(map(int, zk_topic_sylog))
    set_topic_apache = set(map(int, zk_topic_apache))

    size_cluster = len(set_broker)
    size_topic_syslog = len(set_topic_syslog) # Referenz Größe im restlichen Code (Sind immer gleich gross)
    size_topic_apache = len(set_topic_apache)

    # Festlegen von Replication Factor
    if size_cluster == 1:
      replication_factor = 1
    elif size_cluster == 2:
      replication_factor = 2
    else:
      replication_factor = 3

    # Komma Liste String von Broker IDs (in manchen Befehelen benötigt)
    broker_string = ','.join(str(s) for s in set_broker)

    print("Size of Cluster: " + str(size_cluster) + " with brokers: " + broker_string )
    print("Number of Partitions #syslog#: " + str(size_topic_syslog))
    print("Number of Partitions #apache#: " + str(size_topic_apache))
    print("Calculated Replication Factor for this period: " + str(replication_factor))

    # ------------ Check Balance & Partition Creation-------------------------------------
    # tests if there is an imbalance between size of cluster and number of partitions

    # create new partiotions ONLY if cluster is bigger than number of partitions in both topics
    if size_cluster > size_topic_syslog:
      # Es soll für die überschüssigen noch eine Partiotion angelegt werden
      new_size_topic = size_topic_syslog + (size_cluster - size_topic_syslog)
      try:
        print(str(new_size_topic))
        command_scale_syslog = "./bin/kafka-topics.sh --zookeeper localhost:2181 --alter --topic syslog --partitions " + str(new_size_topic)
        command_scale_apache = "./bin/kafka-topics.sh --zookeeper localhost:2181 --alter --topic apache --partitions " + str(new_size_topic)
        output_scale_syslog = subprocess.getoutput(command_scale_syslog)
        output_scale_apache = subprocess.getoutput(command_scale_apache)
        print("######## Output from kafka-topics.sh --alter ####################")
        print(output_scale_syslog)
        print(output_scale_apache)
        print("-----------------------------------------------------------------")
      except:
        print("Error: Something went wrong while creating new partitions")
    else:
      print("!!!!!!! No need for new partition !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    # ------------ Reassignment Management-------------------------------------------------
    # DEFAULT => check if there is a new strategie for reassignment when size_cluster != size_topic
    # kafka-reassign-partitions.sh generates new suggestion
    try:
      # Wird auf Grund nachfolgender eigener Logik nicht mehr benötigt.

      # Code Baustelle ######################################################################
      # ToDo:
      #   - Aktuell wird davon ausgegangen, dass das Cluster durchgehende ID Folge hat 0,1,2,3,...
      #     Augabe: Einfügen von Logik die die Nummerierung missachtet
      #     Erledigt!!! Durch leader/rep = broker_list[index]
      broker_list = sorted(set_broker)
      partition_list = sorted(set_topic_syslog)
      liste_partitions_syslog = []
      liste_partitions_apache = []
      # Erstellt Listen mit Konfigurationen für jede Partition
      for i in partition_list:
        if replication_factor == 1:
          leader = broker_list[(i % size_cluster)]
          liste_partitions_syslog.append('{"topic":"syslog","partition":'+str(i)+',"replicas":['+str(leader)+']}')
          liste_partitions_apache.append('{"topic":"apache","partition":'+str(i)+',"replicas":['+str(leader)+']}')

        elif replication_factor == 2:
          leader = broker_list[(i % size_cluster)]
          rep1 = broker_list[((i+1) % size_cluster)]

          liste_partitions_syslog.append('{"topic":"syslog","partition":'+str(i)+',"replicas":['+str(leader)+','+str(rep1)+']}')
          liste_partitions_apache.append('{"topic":"apache","partition":'+str(i)+',"replicas":['+str(leader)+','+str(rep1)+']}')

        elif replication_factor == 3:
          leader = broker_list[(i % size_cluster)]
          rep1 = broker_list[((i+1) % size_cluster)]
          rep2 = broker_list[((i+2) % size_cluster)]

          liste_partitions_syslog.append('{"topic":"syslog","partition":'+str(i)+',"replicas":['+str(leader)+','+str(rep1)+','+str(rep2)+']}')
          liste_partitions_apache.append('{"topic":"apache","partition":'+str(i)+',"replicas":['+str(leader)+','+str(rep1)+','+str(rep2)+']}')

      # Konkateniert die Elemente der Listen zu JSON
      output_generate_suggestion_with_newReplica = '{"version":1,"partitions":['+(','.join(str(s) for s in liste_partitions_syslog))+','+(','.join(str(s) for s in liste_partitions_apache))+']}'
      # Code Baustelle Ende ##################################################################
      print("####### Generated Reassignment ##################################")
      print(output_generate_suggestion_with_newReplica)
      print("-----------------------------------------------------------------")

      #if old_reassign_suggestion == output_generate_suggestion_with_newReplica:
        #print("There is no change in reassignment suggestion")
      #else:
      output = open('/opt/Kafka/kafka/config/newPartitionAssignment.json', 'w')
      output.write(output_generate_suggestion_with_newReplica)
      # short sleep to be sure for teminated write process
      time.sleep( 3 )
      output.close()
      print("!!!!!!! A new newPartitionAssignment.json content created !!!!!!!")

      if old_reassign_suggestion != output_generate_suggestion_with_newReplica:

        old_reassign_suggestion = output_generate_suggestion_with_newReplica

        # short sleep to be sure that the startup of new instance is complete
        time.sleep( 100 )

        command_execute_suggestion = "./bin/kafka-reassign-partitions.sh --zookeeper localhost:2181  --execute --reassignment-json-file config/newPartitionAssignment.json"
        output_execute_suggestion = subprocess.getoutput(command_execute_suggestion)

        print("####### Output from kafka-reassign-partitions.sh --execute ######")
        print(output_execute_suggestion)
        print("-----------------------------------------------------------------")

        command_execute_leader_election = "bin/kafka-preferred-replica-election.sh --zookeeper localhost:2181"
        output_execute_leader_election = subprocess.getoutput(command_execute_leader_election)

        print("####### Output from kafka-preferred-replica-election.sh  ######")
        print(output_execute_leader_election)
        print("-----------------------------------------------------------------")

      else :
        print("NO CHANGES")

    except:
      print("Error: Something went wrong while reassignment management")

    #-------------- Timeout ----------------------------------------------------------------
    # reassignment every 300 sec if there is the need
    print("Wait ... ... ... \n \n")
    time.sleep( 120 )
  except:
    print("Error: Something went wrong during process!!!")

sys.exit()
