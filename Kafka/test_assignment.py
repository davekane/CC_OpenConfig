import math

replication_factor = 3
set_broker = [0,1,2,4]
size_cluster = 4
set_topic_syslog = [0,1,2,3,4]

liste_partitions_syslog = []
liste_partitions_apache = []
for i in set_topic_syslog:
  if replication_factor == 1:
    # broker (i mod size_cluster) leader
    leader = set_broker[(i % size_cluster)]

    liste_partitions_syslog.append('{"topic":"syslog","partition":'+str(i)+',"replicas":['+str(leader)+']}')
    liste_partitions_apache.append('{"topic":"apache","partition":'+str(i)+',"replicas":['+str(leader)+']}')
  if replication_factor == 2:
    # broker (i mod size_cluster) leader
    # broker (i+1 mod size_cluster) replica
    # broker (i+2 mod size_cluster) replica
    leader = set_broker[(i % size_cluster)]
    rep1 = set_broker[((i+1) % size_cluster)]

    liste_partitions_syslog.append('{"topic":"syslog","partition":'+str(i)+',"replicas":['+str(leader)+','+str(rep1)+']}')
    liste_partitions_apache.append('{"topic":"apache","partition":'+str(i)+',"replicas":['+str(leader)+','+str(rep1)+']}')
  if replication_factor == 3:
    # broker (i mod size_cluster) leader
    # broker (i+1 mod size_cluster) replica
    leader = set_broker[(i % size_cluster)]
    rep1 = set_broker[((i+1) % size_cluster)]
    rep2 = set_broker[((i+2) % size_cluster)]

    liste_partitions_syslog.append('{"topic":"syslog","partition":'+str(i)+',"replicas":['+str(leader)+','+str(rep1)+','+str(rep2)+']}')
    liste_partitions_apache.append('{"topic":"syslog","partition":'+str(i)+',"replicas":['+str(leader)+','+str(rep1)+','+str(rep2)+']}')

output = '{"version":1,"partitions":['+(','.join(str(s) for s in liste_partitions_syslog))+','+(','.join(str(s) for s in liste_partitions_apache))+']}'
print(output)
