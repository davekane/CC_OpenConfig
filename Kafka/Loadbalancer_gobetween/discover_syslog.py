#!/usr/bin/env python3

import os
import sys
import subprocess
import netifaces as ni

output_online_syslog = subprocess.getoutput("nmap -sT 192.168.0.0/24 -p 2323 --open | grep -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' -o")
syslog_server_list = output_online_syslog.split('\n')

own_ip = ni.ifaddresses('ens3')[2][0]['addr']

for ip in syslog_server_list:
    if ip != own_ip:
        sys.stdout.write(ip+":2323 weight=1\n")
