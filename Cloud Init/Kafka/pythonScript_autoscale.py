#!/usr/bin/env python

#ToDo:
#- Anpassung des Scripts zur Realisierung von mehreren Brokern auf einem Node
#	- Anlegen von /tmp directory für broker
#	- Entsprechende Anpassungen in serverRuntime.properties realisieren
#		- host.name muss individuell sein
#		- Port für broker muss individuell sein

import os
import sys
import subprocess
import shlex
import logging
import re
import netifaces as ni
from kazoo.client import KazooClient

logging.basicConfig()

zk = KazooClient(hosts='192.168.0.19:2181')
zk.start()
try:
    #GET ID for broker.id
    try:
        zk_broker_ids = zk.get_children('/brokers/ids')
        zk.stop()
        set_broker_ids = set(map(int, zk_broker_ids))
        possible_broker_ids = set(range(100))

    except:
        propertie_broker_id = 'broker.id=0'
        print("ERROR: Something went wrong while allocating broker.id - Default of 0 will be used !!!")
sys.exit()
