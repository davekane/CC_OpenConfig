#!/usr/bin/env python

# - Communication processing messages
# - #1 I am new - Please set my unique name
# - #2 I already have my unique name and this is my refresh state
# - #3 Server: Set new unique name to client
# - #4 Server: Delete fake DNS entry from all clients
# - #5 Server: Set fake DNS entry in host file (actually one entry per message - Array?)

import socket
import time
import threading
import re
from tempfile   import mkstemp
from shutil     import move
from os         import fdopen, remove

#RegEx patterns to filter DNS Entries
patternAll      = "('(.){1,10}')\: (\('((([1-9]{1,3}\.){3}([1-9]{1,3})))', ([0-9]){1,5}\))"
patternDNS      = "(([a-z]{1,10})([1-9]{1}))"
patternIP       = "(([1-9]{1,3}\.){3}([1-9]{1,3}))"
deletePattern   = "(\{('(.){1,}'): ('delete')\})"

filePath = "/etc/hosts"

#Server Socket Information
TCP_IP = '192.168.0.103'
TCP_PORT = 55235

#Max amount of incoming data which will be accepted
BUFFER_SIZE = 1024
#NetIdent - Which type of group this node is responsible to
whoAmI = "elastic"
#NetIdent - Unique identifier received from server
whoAmINow = "nobody"

#Mandatory messages to login on our DNS service
sayHello = "#1 " + whoAmI
refreshMyState = "#2 "

#Do I already send the welcome message?
sendGreetings = 0

#TCP Socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def networkProcessing():
    try:
        global sendGreetings
        s.connect((TCP_IP, TCP_PORT))
        while 1:
            if sendGreetings == 0:
                startCleaning()
                s.send(sayHello.encode())
                sendGreetings = 1
            else :
                s.send(refreshMyState.encode())

            data = s.recv(BUFFER_SIZE)
            decodedData = str(data.decode())

            #Which type of message was received? (only #3 - #5 are possible)
            if "#3" in decodedData:
                newIdentityReceived(decodedData)
            elif "#4" in decodedData:
                print ("Message: " + decodedData)
                #deleteHostEntry(decodedData)
            elif "#5" in decodedData:
                print ("Message: " + decodedData)
                #dataProcessing(decodedData)
            time.sleep(5)

        s.close()
    except Exception as e:
        s.close()
        print ("Socket closed dude. Lets have a look at this: {}", str(e))

def newIdentityReceived(data):
    global whoAmINow
    global refreshMyState

    localData = data[3:]
    whoAmINow = localData
    refreshMyState += whoAmINow
    print ("I am: " + whoAmINow)

# Message: #5 elastic1 - ('192.168.52.133', 36378)
# (\'(.){1,10}\')\: (\(\'((([1-9]{1,3}\.){3}([1-9]{1,3})))\'\, ([0-9]){1,5}\))
def dataProcessing(dnsInformation):
    localMessage = dnsInformation
    result = re.search(patternAll,localMessage)
    while result != None:
        resultString = result.group()
        getSingleEntry(resultString)
        localMessage = localMessage.replace(resultString, "")
        result = re.search(patternAll,localMessage)

def deleteHostEntry(dnsInformation):
    localMessage = dnsInformation
    result = re.search(deletePattern,localMessage)
    while result != None:
        resultString = result.group()
        getSingleDelEntry(resultString)
        localMessage = localMessage.replace(resultString, "")
        result = re.search(deletePattern,localMessage)


def getSingleDelEntry(dnsSingle):
    dnsEntry    = getRegFromLine(patternDNS, dnsSingle)
    changeHosts(dnsEntry, "0.0.0.0", 1)


def getSingleEntry(dnsSingle):
    dnsEntry    = getRegFromLine(patternDNS, dnsSingle)
    ipEntry     = getRegFromLine(patternIP, dnsSingle)
    changeHosts(dnsEntry, ipEntry, 0)

def changeHosts(host, ip, delFlag):
    if host in open(filePath).read():
        replace(host, ip, delFlag)
    else:
        insertEntryToHosts(host, ip)

def replace(host, ip, delFlag):
    file_path   = filePath
    pattern     = ""
    subst       = ""
    reDelete    = 0
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if host in line:
                    if delFlag == 1:
                        if "#" not in line:
                            pattern = host
                            subst = "#" + host
                    else:
                        if ip not in getRegFromLine(patternIP, line):
                            pattern = getRegFromLine(patternIP, line)
                            subst   = ip
                        if "#" in line:
                            reDelete = 1
                new_file.write(line.replace(pattern, subst))
                pattern = ""
                subst   = ""
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

    if reDelete == 1:
        deComment(host)

def deComment(host):
    file_path   = filePath
    pattern     = ""
    subst       = ""
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if host in line:
                    if "#" in line:
                        pattern = "#" + host
                        subst = host
                new_file.write(line.replace(pattern, subst))
                pattern = ""
                subst   = ""
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

def startCleaning():
    replace("elastic", "0.0.0.0", 1)
    replace("kibana", "0.0.0.0", 1)
    replace("logstash", "0.0.0.0", 1)

def insertEntryToHosts(host, ip):
    with open(filePath, "a") as myfile:
        myfile.write(host + "   " + ip + "\n")

def getRegFromLine(regEx, toReg):
    try:
        tmpReg = re.search(regEx, toReg)
        returnReg = tmpReg.group()
        return returnReg
    except:
        return "null"

#Let's start our elastic client
try:
    networkProcessing()
except Exception as e:
    print ("Error while starting Client: " + str(e))
