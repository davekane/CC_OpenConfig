import socket
import threading
from threading import Timer
import re
import sys
import time
from struct import *

# - Communication processing messages
# - #1 I am new - Please set my unique name
# - #2 I already have my unique name and this is my refresh state
# - #3 Server: Set new unique name to client
# - #4 Server: Delete fake DNS entry from all clients
# - #5 Server: Set fake DNS entry in host file (actually one entry per message - Array?)

#Server Socket Information
host = "192.168.0.103"
port = 55235
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#Global Exit Mode
exitActive = 1

#Modulus value for elastic group
elasticCnt  = 1
kibanaCnt   = 1
logstash    = 1

#HashMap which stores active clients and their IPs
#Auto remove if no refresh was received
isActive = {}

#HashMap which stores the time stamp from the last refresh
refreshList = {}

networkList = {}

def listen():
    sock.bind((host, port))
    #Max simultan conns in queue (elastic -> 6, kibana -> 2, LB -> ?, LogStash -> ?)
    sock.listen(8)

    #Start multithreaded client processing (every client gets a new thread)
    while exitActive:
        client, address = sock.accept()
        client.settimeout(None)
        #client.settimeout(60)
        threading.Thread(target = listenToClient,args = (client,address)).start()

#Client specific data processing
def listenToClient(client, address):
    size = 1024
    while exitActive:
        try:
            data = client.recv(size)
            if data:
                stringData = str(data.decode())
                #There are only two messages a client can send to our DNS service
                if "#1" in stringData:
                    clientName = registrationProcess(stringData, address)
                    client.send(("#3 {}".format(clientName)).encode())

                    #Stores the connection to our clients to reach them in later steps
                    networkList[clientName] = client
                    print ("Stored: " + clientName)
                elif "#2" in stringData:
                    refreshProcess(stringData)

        except:
            client.close()
            return False
        time.sleep(1)

def registrationProcess(clientData, address):
    #Filter current client name (should be elastic or kibana)
    clientName = clientData[3:]
    global elasticCnt

    if "elastic" == clientName:
        newCount = elasticCnt
        newCount = newCount % 7
        currentName = "elastic"

        if newCount == 0:
            newCount += 1
        currentName += str(newCount)

        #Save the new node to active protection
        activeManipulation(currentName, address)

        #Set new elasticValue
        elasticCnt = newCount + 1
        return currentName
    elif "kibana" == clientName:
        newCount = kibanaCnt
        newCount = newCount % 7
        currentName = "kibana"

        if newCount == 0:
            newCount += 1
        currentName += str(newCount)

        #Save the new node to active protection
        activeManipulation(currentName, address)

        #Set new elasticValue
        kibanaCnt = newCount + 1
        return currentName
    else:
        return 0


def activeManipulation(clientData, address):
    if clientData in isActive:
        if isActive[clientData] == None:
            isActive[clientData] = address
            print ("Entry new")
        else:
            print ("No changes are processed")
    else:
        isActive[clientData] = address


def refreshProcess(clientData):
    try:
        #Filter current client name (should be elastic or kibana)
        clientName = clientData[3:]
        #Save current timestamp with DNS entry
        currTS = time.time()
        refreshList[clientName] = currTS
        print (clientName + " passed at: " + str(currTS))
        return 1
    except Exception as e:
        print ("Oh no! There was something wrong at the refresh processing. Lets have a look at this: " + str(e))
        return 0

def activeProtection():
    #Parse refreshList and check if there is any entry that is older than 15 seconds
    #In this case this entry has to be removed from activeList -> Removes this entry from the client
    toDelete = {}
    for entry in refreshList:
        currTS = time.time()

        #Delete any entry which is older than 10 seconds. Avoids any problems on the clients
        if refreshList[entry] != None:
            if (currTS - refreshList[entry]) >= 10:
                refreshList[entry] = None
                isActive[entry] = None
                print ("Delete inactive client: " + str(entry))
                toDelete[entry] = "delete"
                networkList[entry] = None

    #Notify every client on our active list about all the DNS clients we've stored in our active list (its confusing sorry ;) )
    try:
        if len(toDelete) > 0:
            deleteOut(toDelete)
        cryOut()
    except Exception as e:
        print ("Possible I've detected a broken pipe. Let's have a look at this: {}", str(e))
    timerStart()

def timerStart():
    Timer(15, activeProtection, ()).start()

def deleteOut(toDelete):
    for entryOut in networkList:
        if networkList[entryOut] != None:
            networkList[entryOut].send(("#4 {} --CUT--".format(toDelete)).encode())

def cryOut():
    for entryOut in networkList:
        if networkList[entryOut] != None:
            networkList[entryOut].send(("#5 {} --CUT--".format(isActive)).encode())


#Lets start the fake DNS service
try:
    timerStart()
    listen()
except (KeyboardInterrupt, Exception) as e:
    print ("Exception")
    exitActive = 0
    quit(0)
    print ("Closed main socket - This should not happened in production. Lets have a look at this: " + str(e))
